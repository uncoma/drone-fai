# Consultar la Documentación

## Sin instalar

Se puede utilizar un explorador Web. Por ejemplo, firefox:

```
firefox doc/index.html
```

O usando RI:

```
ri -d ./ri DroneFai
ri -d ./ri
```

## Instalar documentación
Utilizar RDoc para instalar en tu sistema de la siguiente manera:

```
rdoc -a --ri
```

## Regenerar la documentación
Para regenerar la documentación es utiliza RDoc:

```
rdoc -a --ri -o ./ri
rdoc -a
```
