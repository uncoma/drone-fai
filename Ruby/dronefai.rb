# Copyright 2023 Christian Gimenez
#
# dronefai.rb
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# frozen_string_literal: true

require 'socket'

# Clases y métodos para gestionar un dron.
module DroneFai
  # Representación del Dron
  #
  # Mantiene la comunicación con el dron.
  class Dron
    # Intervalo de espera entre el envío de un paquete UDP y la recepción
    # de la respuesta.
    SLEEP_INTERVAL = 0.2

    # Iniciar una instancia de Dron.
    #
    # @param ip String IP del dron.
    # @param port Number Puerto UDP para enviar comandos al dron.
    # @param localip String IP local para escuchar el estado del dron.
    # @param srcport Number Puerto UDP para escuchar el estado del dron.
    def initialize(drone_address = { ip: '192.168.10.1', port: 8889 },
                   local_address = { ip: '0.0.0.0', port: 12_345 })
      @socket = UDPSocket.new
      @destino = drone_address
      @fuente = local_address
      @socket.bind local_address[:ip], local_address[:port]

      @keep_alive = crear_keep_alive_thread
      @estado = StatusReceiver.new local_address[:ip]
      @estado_actual = nil
      @estado_thread = crear_estado_thread

      enviar 'command'
    end

    attr_reader :destino, :fuente, :estado, :estado_actual

    # Enviar el mensaje al dron y esperar su respuesta.
    #
    # @param mensaje String El mensaje a enviar.
    # @return String La respuesta del dron.
    def enviar(mensaje)
      @socket.send mensaje, 0, @destino[:ip], @destino[:port]
      respuesta = @socket.recvfrom 1024
      sleep SLEEP_INTERVAL

      respuesta.first
    end

    # Despegar el dron.
    #
    # Envía el comando de despegue.
    def despegar
      enviar 'takeoff'
    end

    def aterrizar
      enviar 'land'
    end

    def adelante(cms = 20)
      mover 'forward', cms
    end

    def atras(cms = 20)
      mover 'back', cms
    end

    def arriba(cms = 20)
      mover 'up', cms
    end

    def abajo(cms = 20)
      mover 'down', cms
    end

    def izquierda(cms = 20)
      mover 'left', cms
    end

    def derecha(cms = 20)
      mover 'right', cms
    end

    def stop
      enviar 'stop'
    end

    def emergency
      enviar 'emergency'
    end

    # Hacer un flip en la direccion indicada.
    #
    # @param direction Symbol Uno de los siguientes símbolos:
    #        `:izquierda, :derecha, :adelante, :atras, :izq, :der`.
    def flip(direccion = :adelante)
      strdir =
        case direccion
        when :izquierda, :izq then 'l'
        when :derecha, :der then 'r'
        when :adelante then 'f'
        when :atras then 'b'
        else 'f'
        end

      enviar "flip #{strdir}"
    end

    def rotar(grados = 25)
      return false if grados < -360 || grados > 360 || grados.zero?

      if rotar.positive?
        enviar "cw #{rotar}"
      else
        enviar "ccw #{rotar}"
      end
    end

    # Cambiar la velocidad del dron.
    #
    # @param cms Number Velocidad en cm/s.
    def velocidad(cms = 10)
      enviar "speed #{cms}"
    end

    def velocidad?
      asegurar_num(enviar('speed?'))
    end

    def bateria?
      asegurar_num(enviar('battery?'))
    end

    def wifi?
      enviar 'wifi?'
    end

    def sdk?
      enviar 'sdk?'
    end

    def sn?
      enviar 'sd?'
    end

    alias izq izquierda
    alias der derecha

    private

    def crear_keep_alive_thread
      Thread.new do
        loop do
          sleep 10
          enviar 'command'
        end
      end
    end

    def crear_estado_thread
      Thread.new do
        loop do
          @estado_actual = @estado.recibir_estado
        end
      end
    end

    def asegurar_num(str)
      if str.match?(/[[:digit:]]+/)
        str.to_i
      else
        false
      end
    end

    def mover(direccion, cms)
      return false if cms < 20 && cms > 500

      enviar "#{direccion} #{cms}"
    end
  end

  # Representa el estado del dron
  class Estado
    def initialize
      @time = 0
      @h = 0
      @bat = 0
    end

    attr_accessor :time, :h, :bat

    class << self
      # Parsear un string con los datos del estado del dron.
      #
      # Crear una instacia de Estado a partir de los datos del string.
      #
      # @param str String
      # @return Estado
      def parse_str(str)
        arr = str.split ';'
        arr.map! { |elt| elt.split ':' }

        st = Estado.new
        st.time = arr.assoc('time')[1].to_i
        st.h = arr.assoc('h')[1].to_i
        st.bat = arr.assoc('bat')[1].to_i

        st
      end
    end
  end

  # Recibe el estado y lo parsea.
  class StatusReceiver
    # Inicializar una instancia de StatusReceiver.
    #
    # @param ip String La IP local.
    # @param port Number el número de puerto a abrir.
    def initialize(ip = '0.0.0.0', port = 8890)
      @socket = UDPSocket.new
      @fuente = { ip: ip, port: port }
      @socket.bind ip, port
    end

    attr_reader :fuente

    # Recibir el estado del dron.
    #
    # Recibir un string por UDP y parsearlo. Retornar una instancia de
    # Estado.
    #
    # @return Estado
    def recibir_estado
      respuesta = @socket.recvfrom 1024
      Estado.parse_str respuesta.first
    end
  end
end
