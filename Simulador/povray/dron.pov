/* 
Copyright 2023 Christian Gimenez

Author: Christian Gimenez   

dron.pov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "colors.inc"
#include "woods.inc"

#include "povray/positions.pov"

camera {
  // location <0, 20, -10>
  location CAMPOS
  look_at DRONEPOS
}

light_source { <2, 100, -3> color White}

box {
  <-100, -0.5, -100>,
  <100, 0, 100>
  texture {
    T_Wood2
  }
}

cylinder {
  <0,0,0>,
  <0,0.1,0>,
  1
  texture { pigment { Green } }
}

union {
  box{
    <-0.25, 0, -1>,
    <0.25, 0.25, 0.5>
    texture { pigment { color rgb <0.1, 0.1, 0.2>} }
  }
  prism {  
    0, 0.25, 3,
    <-0.25, 0.5>, <0.25, 0.5>, <0, 1>
    texture {pigment { Yellow }}
    //translate <0, 0, 0.75>
  }


  // Adelante
  torus {
    0.5, 0.1
    translate <-0.75, 0.35, -1.0>
    texture { pigment { color rgb <0.1, 0.1, 0.2>} }
  }
  torus {
    0.5, 0.1
    translate <0.75, 0.35, -1.0>
    texture { pigment { color rgb <0.1, 0.1, 0.2>} }
  }
  // Atrás
  torus {
    0.5, 0.1
    translate <-0.75, 0.35, 0.75>
    texture { pigment { color rgb <0.1, 0.1, 0.2>} }
  }
  torus {
    0.5, 0.1
    translate <0.75, 0.35, 0.75>
    texture { pigment { color rgb <0.1, 0.1, 0.2>} }
  }
  
  

  rotate DRONEROT
  translate DRONEPOS
}
