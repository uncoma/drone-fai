/*   tello_povray
     Author: Gimenez, Christian.

     Copyright (C) 2023 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     dic 2023
*/

:- module(tello_povray, [
              write_position_to_file/1,
              generate_position/1,
              generate_camera/1,
              generate_image/0
          ]).

/** <module> Exportar el estado del dron a povray.

Crea los scripts necesarios para setear la posición y llama al programa
`povray` sobre el archivo "povray/dron.pov". Esto genera una imágen PNG
con la figura en 3D del dron.

@author Christian Gimenez
@license GPLv3
*/

:- use_module(tello_actions).

%! write_position_to_file(+FilePath: atom)
%
% Escribi la posición del dron y la cámara en el archivo indicado.
write_position_to_file(FilePath) :-
    generate_position(PosStr),
    generate_camera(CamStr),
    
    open(FilePath, write, Out),
    write(Out, PosStr),
    write(Out, CamStr),
    close(Out).

%! generate_position(-Str: string) is det.
%
% Generar la posición del dron en un string para povray.
generate_position(Str) :-
    drone_state(pos(X,Z)),
    drone_state(height(Y)),
    
    format(string(Str),
           "#declare DRONEPOS=<~f,~f,~f>;\n#declare DRONEROT=<0,0,0>;\n",           
           [X, Y, Z]).

%! generate_camera(-Str: String) is det.
%
% Calcular la posición de la cámara para ver el dron en povray.
generate_camera(Str) :-
    drone_state(pos(X,Z)),
    drone_state(height(Y)),

    Y2 is Y + 20,
    Z2 is Z - 10,
    
    format(string(Str),
           "#declare CAMPOS=<~f,~f,~f>;\n",
           [X, Y2, Z2]).

%! generate_image is det.
%
% Generar la imágen dron.png ejecutando el programa povray.
generate_image :-
    write_position_to_file('povray/positions.pov'),
    shell('povray povray/dron.pov').
