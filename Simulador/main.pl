/*   main.pl
     Author: Gimenez, Christian.

     Copyright (C) 2023 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     dic 2023
*/

:- module(main, [
              drone_address/2,
              main/0
          ]).
/** <module> Módulo principal para simular el dron.

Este módulo se puede ejecutar como un script en la terminal de la
siguiente manera:

```
swipl main.pl
```

Gestiona la entrada y salida estándar. Simula el funcionamiento de los
puertos UDP del dron. El módulo tello_actions es el que simula el
estado y los mensajes del dron.

Los módulos se encuentran documentados como se indica en PLDoc. Se
puede consultar usando en navegador Web iniciando swipl y dentro del
REPL, ejecutar:

```
use_module(main).
doc_browser.
```

@author Christian Gimenez
@license GPLv3
*/
:- use_module(tello_actions).
:- use_module(tello_povray).

:- initialization(main, main).

%! drone_address(?IP: atom, ?Puerto: number) is det.
%
% La dirección IP y puerto del dron a simular.
drone_address('0.0.0.0', 8889).
drone_state_port(8890).

%! client_address(?Address: term)
%
% Almacenar la dirección del cliente.
%
% El dron solo responde a la IP y Puerto que recibe el primer command.
% No responde a ninguna otra dirección más. Con este predicado se
% simula este comportamiento.
:- dynamic(client_address/1).

%! state_string(-Str: string)
%
% Retornar un string con el estado del dron que se encuentra almacenado
% en drone_state/1.
state_string(Str) :-
    findall(State, drone_state(State), Bag),
    format(string(Str), '~q', [Bag]).

%% No es necesario, porque falla en process_input_int/3.
%% answer_client(Socket, Output, FromAddress) :-
%%     client_address(FromAddress),
%%     udp_send(Socket, Output, FromAddress, [encoding(utf8)]),
%%     format('Dron > ~q', [Output]), nl.

%! process_input_int(+Address, +Input, -Output)
%
% Procesar entrada si el cliente está registrado.
%
% Procesar la entrada si:
% 1. El cliente está registrado en client_address/1.
% 2. El cliente envía por primera vez 'command'. En este caso, se registra al cliente.
%
% Se falla si la dirección del cliente no coincide con la dirección registrada.
process_input_int(FromAddress, Input, Output) :-
    %% El cliente ya fue registrado y coincide.    
    client_address(FromAddress), !,

    process_input(Input, Output).
process_input_int(FromAddress, 'command', Output) :-
    %% El cliente emite command por primera vez.
    write('Dron > Primer command: Cliente registrado.'), nl,
    
    asserta(client_address(FromAddress)), !,
    process_input('command', Output).
process_input_int(_, _, _) :-
    write('Dron > Cliente denegado.'), nl,
    
    fail.

%! main_process(+Socket)
%
% Implementar el REPL del dron sobre el socket UDP.
%
% Repetir incansablemente la simulación de la recepción del comando
% del cliente, la ejecución del mismo y el envío del mensaje resultante
% del dron.
%
% En la salida estándar se reporta cada acción. Se escribe con "Dron >"
% la respuesta y mensajes del mismo y con el formato
% "ip(A,B,C,D):Puerto > " lo recibido por el cliente.
main_process(Socket) :-
    repeat,
    
    state_string(StateStr),
    format('Dron > Estado: ~q', StateStr), nl,
    udp_receive(Socket, Input, FromAddress, [as(atom), encoding(utf8)]),
    format('~q > ~q', [FromAddress, Input]), nl,
    process_input_int(FromAddress, Input, Output),
    udp_send(Socket, Output, FromAddress, [encoding(utf8)]),
    format('Dron > ~q', [Output]), nl,
    
    fail. %% Fallar hace que repita en repeat/0.

client_command_thread :-
    drone_address(IP, Puerto),
    
    udp_socket(Socket),
    tcp_bind(Socket, IP:Puerto),
    main_process(Socket).

%! saludo is det.
%
% Imprimir en la salida estándar un mensaje de saludo.
saludo :-
    drone_address(IP, Puerto),
    
    write('¡Hola! Este es un simulador de un dron.'), nl,
    format('La IP de este dron es ~q y el puerto es ~q.', [IP, Puerto]), nl,
    write('El dron se ha encendido.'), nl.

%! main
%
% Programa principal del script. Todo inicia aquí.
main :-
    saludo,
    start_simulation,

    thread_create(state_port_thread, _),
    thread_create(client_command_thread, _),

    main_prompt.

%! execute_command(+Command: term)
%
% Ejecutar un comando del simulador.
%
% Comandos disponibles son: quit, no_idle, no_battery, status, povray.
execute_command(quit) :- !,
    halt.
execute_command(no_idle) :- !,
    asserta(drone_state(no_idle)).
execute_command(no_battery) :- !,
    retractall(drone_state(battery(_))),
    asserta(drone_state(battery(9999999))).
execute_command(status) :- !,
    state_string(StateStr),
    format('Dron > Estado: ~q', StateStr), nl.
execute_command(povray) :- !,
    generate_image.
execute_command(_) :- !,
    write('Comando no reconocido.'), nl.

%! main_prompt
%
% Repetitiva utilizada para obtener comandos del simulador del usuario.
%
% @see execute_command/1
main_prompt :-
    repeat,

    write('Escribe el comando a ejecutar.'), nl,
    read(Command),
    execute_command(Command),
    
    fail.
    

%! state_port_thread is det.
%
% Gestiona el puerto UDP y envía contínuamente el estado al cliente.
%
% Este predicado está pensado para usarse como Goal para create_thread/2.
%
% # Nota del simulador:
%
% El dron envía desde el puerto UDP 192.168.10.1:8889 al cliente en
% 192.168.10.2:8890. O sea, el dron envía paquetes UDP con puerto origen
% 8889 y destino 8890. Usa para enviar el mismo puerto en el que está
% escuchando, lo cual técnicamente es un toque raro.
%
% En Prolog, no se permite compartir sockets por argumento entre
% threads, ya que se copian y sería otro socket. Esto se podría
% arreglar con un predicado dinámico. Sin embargo, utilizar otro puerto
% origen no afecta a la simulación y los clientes deben responder de
% igual manera.
%
% @tbd Utilizar el mismo socket con puerto 8889 (el que escucha).
state_port_thread :-    
    udp_socket(Socket),    
    write('State port iniciado.'), nl,

    state_port_repeat_process(Socket).

%! state_port_repeat_process(+Socket)
%
% Predicado que repite el envío del estado al cliente.
%
% El dron envía al cliente el estado por cada 10Hz (0.06 segundos),
% según el manual. Este es un string enviado por UDP a la IP del
% cliente y su puerto debe ser 8890. Esto solo se activa luego de
% de que el dron reciba "command".
state_port_repeat_process(Socket) :-
    repeat,

    sleep(0.06),
    
    drone_state(first_command), %% si falla, repite.
    client_address(FromIP:_Port), %% si falla, repite.
    drone_state_port(State_Port),

    generate_state(Str),
    udp_send(Socket, Str, FromIP:State_Port, [encoding(utf8)]),
    
    fail.
