/*   tello_actions.pl
     Author: Gimenez, Christian.

     Copyright (C) 2023 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     dic 2023
*/

:- module(tello_actions, [
              start_simulation/0,
              process_input/2,
              drone_state/1,
              generate_state/1
          ]).
/** <module> Simula las acciones y el estado del dron.

Este módulo define predicados estáticos y dinámicos que simulan las
acciones del dron y el estado del mismo. Las acciones y respuestas a
los mensajes recibidos las procesa process_input/2. El puerto UDP y
lo referido al socket no se simulan en este módulo.

Para inicializar el estado del dron, se debe utilizar
start_simulation/0 al comenzar el programa. Este predicado también
crea un hilo para simular el apagado después de cierta cantidad de
segundos sin actividad.

@author Christian Gimenez
@license GPLv3

@tbd Simular el tiempo de vuelo.
@tbd Faltan varios comandos.
*/

%! drone_state(State: term)
%
% Guarda el estado del dron.
% Los estados son:
% - `first_command` : Se envió aunque sea un command. Necesario según el manual.
% - `landed` : El dron está reposando.
% - `took_of` : El dron está en vuelo.
% - `pos(X,Z)` : Posición del dron.
% - `height(Height)` : Altura del dron (o posición Y).
% - `shut_down` : El dron se apagó. Usualmente sucede luego de pasados 15/30 segundos sin actividad.
% - `shutdown_timer(Time)` : Hora en que el dron debe apagarse en caso de inactividad.
% - `no_idle` : Ignorar shutdown_timer (no se apaga después del tiempo IDLE).
% - `wifi(ESSID, Password)` : Nombre y clave del la red Wi-fi generado por el dron.
% - `time(FlyTime)` : Tiempo de vuelo (en segundos).
% - `battery(Porcentaje)` : Porcentaje de carga de la batería.
% - `speed(Velocidad)` : Velocidad de movimiento del dron en cm/s.
:- dynamic(drone_state/1).

%! shutdown_max_time(?Seconds: number) is det.
%
% Cantidad de segundos para que el dron se apague si no recibe un comando.
shutdown_max_time(30).

%! check_shutdown_timer is det.
%
% Apagar el dron si estuvo mucho tiempo inactivo.
%
% El dron se apaga si shutdown_timer_fine/0 falla. Esto sucede si
% no se recibe un comando cada N segundos. N segundos se obtiene de
% shutdown_max_time/1.
check_shutdown_timer :-
    %% El dron aún tiene tiempo de idle (no se debe apagar).
    shutdown_timer_fine, !.
check_shutdown_timer :-
    %% El dron estuvo mucho tiempo en idle. Apagar.
    write('Dron supero el tiempo IDLE. Se apaga.'), nl,
    halt.

%! shutdown_idle_thread is det.
%
% Predicado utilizado para chequear el temporizador de inactividad.
% Pensado para usarse en un hilo con create_thread/2.
shutdown_idle_thread :-
    repeat,

    shutdown_max_time(Segs),
    check_shutdown_timer,
    sleep(Segs),
    
    fail. %% repetir al fallar.

%! battery_down is det.
%
% Reduce la batería según el uso actual.
%
% Si el dron está en vuelo, reducir la batería más rápido.
%
% Side effect: Modifica drone_state/1.
battery_down :-
    %% Caso: Dron usa el motor y el Wifi.
    drone_state(took_off),
    drone_state(battery(Battery)),

    NewBattery is Battery - 0.10,
    
    retractall(drone_state(battery(_))),
    asserta(drone_state(battery(NewBattery))).
battery_down :-
    %% Caso: Dron no usa el motor, solo usa el Wifi.
    drone_state(landed),
    drone_state(battery(Battery)),

    NewBattery is Battery - 0.05,
    
    retractall(drone_state(battery(_))),
    asserta(drone_state(battery(NewBattery))).

%! battery_down_thread is det.
%
% Predicado utilizado para simular la reducción de la batería.
%
% Si la batería tiene menos del 10%, entonces terminar el programa.
battery_down_thread :-
    drone_state(battery(Battery)),
    Battery =< 10.0, !, halt.
battery_down_thread :-

    repeat,

    battery_down,
    sleep(1),
    
    drone_state(battery(Battery)),
    %% format('Battery down: ~f %', [Battery]), nl,
    Battery =< 10.0, !,
    write('Batería baja. Apagando.'), nl,
    halt.

%! start_simulation is det.
%
% Iniciar la simulación y el estado del dron.
%
% Side effect: drone_state/1.
start_simulation :-
    retractall(drone_state/1),

    assertz(drone_state(pos(0,0))),
    assertz(drone_state(height(0))),
    assertz(drone_state(landed)),
    assertz(drone_state(speed(10))),
    assertz(drone_state(battery(100.0))),
    assertz(drone_state(time(0))),
    assertz(drone_state(wifi('TELLO-0001', ''))),
                            
    reset_shutdown_timer,
    thread_create(shutdown_idle_thread, _),
    thread_create(battery_down_thread, _).

%! shutdown_timer_fine is det.
%
% ¿El dron se activo? ¿No se apagó por inactividad?
%
% Chequea que el tiempo de inactividad no se haya superado.
shutdown_timer_fine :-
    drone_state(no_idle).
shutdown_timer_fine :-
    drone_state(shutdown_timer(TimeToShutdown)),
    get_time(TimeNow),    
    
    TimeNow < TimeToShutdown.

%! reset_shutdown_timer is det.
%
% Resetea el temporizador de apagado. Esto se utiliza para simular el
% apagado del dron por inactividad.
%
% @see shutdown_timer_fine/0
% @see shutdown_max_time/1   
reset_shutdown_timer :-
    shutdown_max_time(MaxTimer),
    get_time(TimeNow),
    TimeToShutdown is TimeNow + MaxTimer,
    
    retractall(drone_state(shutdown_timer(_))),
    assertz(drone_state(shutdown_timer(TimeToShutdown))).

%% Se marcan con "%% no chequeado" si no se verificó ese resultado con
%% el dron.

parse_input(Input, command(Command, Parameters)) :-
    atom_string(Input, Str),
    split_string(Str, " ", "", [CommandStr|Parameters]),
    atom_string(Command, CommandStr).

%! process_input(+Entrada: atom, -Salida: atom) is det.
%
% Procesa la entrada, cambia el estado del dron y retorna la Salida.
process_input(Input, Output) :-
    %% Dron encendido y el comando es correcto.
    shutdown_timer_fine,

    parse_input(Input, Command),
    do_input(Command, Output), !,
    
    reset_shutdown_timer.
process_input(_, 'Invalid command') :- %% Output no chequeado.
    %% Dron encendido, pero se recibió un comando extraño.
    shutdown_timer_fine, !,
    reset_shutdown_timer.
process_input(_, shutdown) :-
    %% El dron ya está apagado.
    halt. %% Sí, sé que está mal... pero bueno...

%! do_input(+Entrada: atom, -Salida: atom)
%
% Definición de qué acciones hacer ante la Entrada dada. No es
% necesario que sea determinístico, pero process_input/2 ignorará los
% demás resultados.
do_input(command('command', []), 'ok') :-
    command.
do_input(command('takeoff', []), 'error') :-
    drone_state(took_off).
do_input(command('takeoff', []), 'ok') :-    
    take_off.
do_input(command('land', []), 'error') :-
    drone_state(landed).
do_input(command('land', []), 'ok') :-
    land.
do_input(command('streamon', []), 'ok').
do_input(command('streamoff', []), 'ok').
do_input(command('emergency', []), 'ok') :-
    emergency.
do_input(command('up', [X]), 'ok') :-
    up(X).
do_input(command('down', [X]), 'ok') :-
    down(X).
do_input(command('left', [X]), 'ok') :-
    left(X).
do_input(command('right', [X]), 'ok') :-
    right(X).
do_input(command('forward', [X]), 'ok') :-
    forward(X).
do_input(command('back', [X]), 'ok') :-
    back(X).
do_input(command('cw', [X]), 'ok') :-
    cw(X).
do_input(command('ccw', [X]), 'ok') :-
    ccw(X).
do_input(command('flip', [_X]), 'ok').
do_input(command('go', [_X, _Y, _Z, _Speed]), 'ok').
do_input(command('stop', []), 'ok').
do_input(command('curve', [_X1, _Y1, _Z1, _X2, _Y2, _Z2, _Speed]), 'ok').
do_input(command('go', [_X, _Y, _Z, _Speed, _Mid]), 'ok').
do_input(command('curve', [_X1, _Y1, _Z1, _X2, _Y2, _Z2, _Speed, _Mid]), 'ok').
do_input(command('jump', [_X, _Y, _Z, _Speed, _Yaw, _Mid1, _Mid2]), 'ok').
do_input(command('speed', [Speed]), 'ok') :-
    speed(Speed).
do_input(command('rc', [ _A, _B, _C, _D]), 'ok').
do_input(command('wifi', [_SSID, _Pass]), 'ok').
do_input(command('mon', []), 'ok').
do_input(command('moff', []), 'ok').
do_input(command('mdirection', [_X]), 'ok').
do_input(command('ap', [_SSID, _Pass]), 'ok').
do_input(command('speed?', []), Speed) :-
    speed_q(Speed).
do_input(command('battery?', []), Battery) :-
    battery_q(Battery).
do_input(command('time?', []), Time) :-
    time_q(Time).
do_input(command('wifi?', []), Wifi) :-
    wifi_q(Wifi).
do_input(command('sdk?', []), SDK) :-
    sdk_q(SDK).
do_input(command('sn?', []), SNR) :-
    sn_q(SNR).

%! height_repeat(+Stop: number, +Direction: atom) is det.
%
% Simula el cambio de altura.
% Simula que aumenta o disminuye la altura del dron hasta llegar a Stop.
%
% Side effect: modifica drone_state/1.
%
% @param Direction Puede ser `up` o `down`.
height_repeat(Stop, up) :-
    repeat,

    sleep(1),
    drone_state(height(Height)),
    drone_state(speed(Speed)),
    New_Height is Height + Speed,    
    retractall(drone_state(height(_))),
    asserta(drone_state(height(New_Height))),
   
    New_Height >= Stop, !.
height_repeat(Stop, down) :-
    repeat,

    sleep(1),
    drone_state(height(Height)),
    drone_state(speed(Speed)),
    New_Height is Height - Speed,
    retractall(drone_state(height(_))),
    asserta(drone_state(height(New_Height))),
   
    New_Height =< Stop, !.


%! up(+X: string) is det.
%
% Moverse hacia arriba.
% Se simula la velocidad del dron considerando los datos del estado.
%
% @see speed/1
% @see speed_q/1
up(X) :-
    drone_state(first_command),
    drone_state(took_off),

    number_string(Num, X),
    Num >= 20, Num =< 500,
    %% drone_state(speed(Speed)),
    %% SleepTime is Num / Speed,
    %% sleep(SleepTime).
    
    drone_state(height(Height)),
    Stop is Height + Num,
    height_repeat(Stop, up).

%! ensure_height(+Height: number, -New_Height: number) is det.
%
% Asegurarse que la altura no sea menor a 10.
ensure_height(Height, Height) :-
    Height > 10, !.
ensure_height(_Height, 10).
    

%! down(+X: string) is det.
%
% Moverse hacia abajo.
% Se simula la velocidad del dron considerando los datos del estado.
%
% @see speed/1
% @see speed_q/1
down(X) :-
    drone_state(first_command),
    drone_state(took_off),
    
    number_string(Num, X),
    Num >= 20, Num =< 500,
    %% drone_state(speed(Speed)),
    %% SleepTime is Num / Speed,
    %% sleep(SleepTime).
    
    drone_state(height(Height)),
    Stop is Height - Num,
    ensure_height(Stop, Stop2),
    height_repeat(Stop2, down).

%! move_xz(+X: number, +Z: number) is det.
%
% Sumar o restar a la posición del dron sobre el plano XZ.
%
% Side effect: drone_state/1.
move_xz(X, Z) :-
    drone_state(pos(X1, Z1)),

    X2 is X1 + X,
    Z2 is Z1 + Z,
    
    retractall(drone_state(pos(_, _))),
    assertz(drone_state(pos(X2, Z2))).

%! left(+X: string) is det.
%
% Moverse hacia la izquierda.
% Se simula la velocidad del dron considerando los datos del estado.
%
% @see speed/1
% @see speed_q/1
left(X) :-
    drone_state(first_command),
    drone_state(took_off),
    
    number_string(Num, X),
    Num >= 20, Num =< 500,
    drone_state(speed(Speed)),

    SleepTime is Num / Speed,
    sleep(SleepTime),

    move_xz(Num, 0).

%! right(+X: string) is det.
%
% Moverse hacia la derecha.
% Se simula la velocidad del dron considerando los datos del estado.
%
% @see speed/1
% @see speed_q/1
right(X) :-
    drone_state(first_command),
    drone_state(took_off),

    number_string(Num, X),
    Num >= 20, Num =< 500,
    drone_state(speed(Speed)),

    SleepTime is Num / Speed,
    sleep(SleepTime),

    move_xz(-Num, 0).

%! forward(+X: string) is det.
%
% Moverse hacia adelante.
% Se simula la velocidad del dron considerando los datos del estado.
%
% @see speed/1
% @see speed_q/1
forward(X) :-
    drone_state(first_command),
    drone_state(took_off),

    number_string(Num, X),
    Num >= 20, Num =< 500,
    drone_state(speed(Speed)),

    SleepTime is Num / Speed,
    sleep(SleepTime),

    move_xz(0, Num).

%! back(+X: string) is det.
%
% Moverse hacia atrás.
% Se simula la velocidad del dron considerando los datos del estado.
%
% @see speed/1
% @see speed_q/1
back(X) :-
    drone_state(first_command),
    drone_state(took_off),

    number_string(Num, X),
    Num >= 20, Num =< 500,
    drone_state(speed(Speed)),

    SleepTime is Num / Speed,
    sleep(SleepTime),

    move_xz(0, -Num).

%! cw(+X: string) is det.
%
% Girar en sentido horario.
% Se simula la velocidad del dron considerando los datos del estado.
%
% @see speed/1
% @see speed_q/1
cw(X) :-
    drone_state(first_command),
    drone_state(took_off),

    number_string(Num, X),
    Num >= 20, Num =< 500,
    drone_state(speed(Speed)),

    SleepTime is Num / Speed,
    sleep(SleepTime).

%! ccw(+X: string) is det.
%
% Girar en sentido anti-horario.
% Se simula la velocidad del dron considerando los datos del estado.
%
% @see speed/1
% @see speed_q/1
ccw(X) :-
    drone_state(first_command),
    drone_state(took_off),

    number_string(Num, X),
    Num >= 20, Num =< 500,
    drone_state(speed(Speed)),

    SleepTime is Num / Speed,
    sleep(SleepTime).

%! emergency is det.
%
% Aterrizar pero sin esperar (se cae el dron).
emergency :-
    drone_state(first_command),

    retractall(drone_state(took_off)),
    retractall(drone_state(landed)),
    assertz(drone_state(landed)).   

%! command is det.
%
% Asegurarse de que el estado first_command esté guardado.
command :-
    drone_state(first_command).
command :-
    assertz(drone_state(first_command)).

%! take_off_repeat
%
% Repetir: por cada un segundo modificar la altura.
% Esto simula el despegue.
take_off_repeat :-
    repeat,

    sleep(1),
    drone_state(height(Height)),
    drone_state(speed(Speed)),
    Height2 is Height + Speed,
    format("taking off: height is ~d cm at ~d cm/s", [Height2, Speed]), nl,
    retractall(drone_state(height(_))),
    asserta(drone_state(height(Height2))),
   
    Height2 >= 70, !.

%! take_off is det.
%
% Simular el despegue y guardar el estado.
take_off :-
    drone_state(first_command),
    drone_state(landed),
    
    take_off_repeat, %% <- simular el despegue
    
    retractall(drone_state(landed)),
    assertz(drone_state(took_off)).

%! land_repeat
%
% Repetir: por cada un segundo modificar la altura.
% Esto simula el aterrizaje.
land_repeat :-
    repeat,

    sleep(1),
    drone_state(height(Height)),
    drone_state(speed(Speed)),
    Height2 is Height - Speed,
    format("landing: height is ~d cm at ~d cm/s", [Height2, Speed]), nl,
    retractall(drone_state(height(_))),
    asserta(drone_state(height(Height2))),
   
    Height2 =< 0, !.

%! land is det.
%
% Simular el aterrizaje y guardar el estado.
land :-
    drone_state(first_command),
    drone_state(took_off),

    land_repeat, %% <- simular el aterrizaje
    
    retractall(drone_state(took_off)),    
    assertz(drone_state(landed)),
    retractall(drone_state(height(_))),
    asserta(drone_state(height(0))).

%! speed(+SpeedStr: String) is det.
%
% Cambiar la velocidad de vuelo.
speed(SpeedStr) :-
    drone_state(first_command),
    number_string(Speed, SpeedStr),
    Speed >= 10, Speed =< 100,

    retractall(drone_state(speed(_))),
    assertz(drone_state(speed(Speed))).

%! speed_q(-Speed: number) is det.
%
% Retornar la velocidad de vuelo.
speed_q(Speed) :-
    drone_state(first_command),
    drone_state(speed(Speed)).

%! battery_q(-Battery: number) is det.
%
% Retornar el porcentaje de la batería.
battery_q(Battery) :-
    drone_state(first_command),
    drone_state(battery(Battery)).

%! time_q(-Time: number) is det.
%
% Retornar el tiempo de vuelo.
time_q(Time) :-
    drone_state(first_command),
    drone_state(time(Time)).

%! wifi_q(-ESSID: atom) is det.
%
% Retornar el nombre del Wifi.
wifi_q(Wifi) :-
    drone_state(first_command),
    drone_state(wifi(Wifi, _)).

sdk_q('2.0') :-
    drone_state(first_command).

sn_q('TELLO-0001') :-
    drone_state(first_command).
    
%! generate_state(-Str: string) is det.
%
% Generar el string de estado.
%
% Este string es el enviado por el dron por el puerto UDP 8890.
generate_state(Str) :-
    drone_state(battery(Battery)),
    Battery2 is round(Battery),
    drone_state(time(Time)),
    drone_state(height(Height)),
    
    format(string(Str),
           "mid:257;x:0;y:0;z:0;mpry:0,0,0;pitch:0;roll:0;yaw:0;vgx:0;vgy:0;vgz:0;templ:92;temph:94;tof:10;h:~d;bat:~d;baro:431.27;time:~d;agx:-11.00;agy:7.00;agz:-998.00;\r\n",
           [
               Height,
               Battery2,
               Time
           ]).
