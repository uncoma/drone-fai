#Autora: Valentina Villarroel :)
import socket
from time import sleep

INTERVAL = 0.2

socketito= None
tello_address = None

def enviar(comandito):
    '''
    Funcion que envia un comando
    '''
    global socketito
    global tello_address
    socketito.sendto(comandito.encode('utf-8'), tello_address)
    response, ip = socketito.recvfrom(1024)
    print (comandito +': ' + str(response))
    sleep(INTERVAL)
    return str(response)

def iniciar():
    '''
    Funcion que inicializa el socket y se conecta al drone
    '''
    global tello_address
    global socketito
    local_ip = ''
    local_port = 12345
    socketito = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # socket for sending cmd
    socketito.bind((local_ip, local_port))

    tello_ip = '192.168.10.1'
    tello_port = 8889
    tello_address = (tello_ip, tello_port)
    enviar('command')
    sleep(INTERVAL)

def despegar():
    '''
    Funcion para despegar
    '''
    enviar('takeoff')
    #if str(response)!='ok':
    #    exit()

def bateria():
    '''
    Funcion para consultar bateria
    '''
    return enviar('battery?')
    #if str(response)!='ok':
    #   exit()

def aterrizar():
    '''
    Funcion para aterrizar
    '''
    enviar('land')
    #if str(response)!='ok':
    #   exit()

def flip(direccion):
    '''
    Funcion para realizar un flip hacia izquierda, derecha, adelante, atras
    '''
    letrita= switchFlip(direccion)
    if letrita=='N':
        default()
    else:
        enviar('flip '+str(letrita))

def rotar(graditos):
    '''
    Funcion que rota x grados
    '''
    if graditos>0:
        enviar('cw '+str(graditos))
    elif graditos<0:
        enviar('ccw '+str(graditos*(-1)))

def detener():
    '''
    Funcion que aborta la ejecucion
    '''
    enviar('emergency')


#Funciones para mover el drone (arriba, abajo, izquierda, derecha, adelante, atras)

def arriba(cm):
    '''
    Funcion que permite mover hacia arriba
    '''
    direccion= 'up'
    verificar(direccion, cm)

def abajo(cm):
    '''
    Funcion que permite mover hacia abajo
    '''
    direccion= 'down'
    verificar(direccion, cm)

def izquierda(cm):
    '''
    Funcion que permite mover hacia izquierda
    '''
    direccion= 'left'
    verificar(direccion, cm)

def derecha(cm):
    '''
    Funcion que permite mover hacia derecha
    '''
    direccion= 'right'
    verificar(direccion, cm)

def atras(cm):
    '''
    Funcion que permite mover hacia atras
    '''
    direccion= 'back'
    verificar(direccion, cm)

def adelante(cm):
    '''
    Funcion que permite mover hacia adelante
    '''
    direccion= 'forward'
    verificar(direccion, cm)


#Funcion alternativa para mover el drone pero pasando la direccion por parametro
def mover(direccion, cm):
    '''
    Funcion que pide como parametro (izquierda, derecha, arriba, abajo, adelante, atras)
    '''
    direccion=switchMover(direccion)
    verificar(direccion, cm)

def verificar(direccion, cm):
    '''
    Funcion que verifica que los cm entren en el rango y envia el comando
    '''
    if (direccion=='N' or cm<20 or cm>500):
        default()
    else:
        enviar(str(direccion)+' '+str(cm))

def default():
    return 'Opcion invalida'

def switchFlip(direccion):
    '''
    Funcion que dada una direccion en string devuelve la letra correspondiente al comando, si es invalido devuelve N
    '''
    letrita='N'
    direccion= direccion.lower()
    if direccion=='izquierda':
        letrita= 'l'
    elif direccion=='derecha':
        letrita='r'
    elif direccion=='atras':
        letrita='b'
    elif direccion=='adelante':
        letrita='f'
    return letrita

def switchMover(direccion):
    '''
    Funcion que dada una direccion en string devuelve el comando correspondiente, si es invalido devuelve N
    '''
    resultado='N'
    direccion= direccion.lower()
    if direccion=='izquierda':
        resultado= 'left'
    elif direccion=='derecha':
        resultado='right'
    elif direccion=='atras':
        resultado='back'
    elif direccion=='adelante':
        resultado='forward'
    elif direccion=='arriba':
        resultado='up'
    elif direccion=='abajo':
        resultado='down'
    return resultado



