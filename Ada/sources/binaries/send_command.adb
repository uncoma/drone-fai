--  send_command.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Drones;
use Drones;

procedure Send_Command is
    procedure Show_Help;
    procedure Send (Command : String; With_Answer : Boolean);

    Drone : Drone_Type;

    procedure Send (Command : String; With_Answer : Boolean) is
        Output : Unbounded_String;
    begin
        Put_Line ("> """ & Command & """");
        if With_Answer then
            Drones.Send_Command_With_Answer (Drone, Command, Output);
        else
            Drones.Send_Command (Drone, Command);
        end if;
        Put_Line ("< """ & To_String (Output) & """");
    end Send;

    procedure Show_Help is
    begin
        Put_Line ("send_command [-w] ""command""");
        New_Line;
        Put_Line ("  -w  Wait for drone answer. By default, do not wait.");
    end Show_Help;

begin
    if Argument_Count < 1 then
        Show_Help;
        return;
    end if;

    if Argument (1) = "-w" then
        Send (Argument (2), True);
    else
        Send (Argument (1), False);
    end if;

end Send_Command;
